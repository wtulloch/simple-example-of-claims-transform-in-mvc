﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Services;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace ClaimsTransformDemo
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AntiForgeryConfig.UniqueClaimTypeIdentifier = ClaimTypes.Name;
        }

        protected void Application_PostAuthenticateRequest()
        {
            if (!ClaimsPrincipal.Current.Identity.IsAuthenticated) return;

            var transformer = FederatedAuthentication.FederationConfiguration
                .IdentityConfiguration
                .ClaimsAuthenticationManager;

            var transformedPrincipal = transformer.Authenticate(string.Empty, ClaimsPrincipal.Current);

            Thread.CurrentPrincipal = transformedPrincipal;
            HttpContext.Current.User = transformedPrincipal;
        }
    }
}
