﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace Demo.SecurityCore
{
    public  static class Extensions
    {
        public static void TryAdd(this IList<Claim> collection, Claim claim)
        {
            if (!collection.Any(c => c.Type == claim.Type && c.Value == claim.Value))
            {
                collection.Add(claim);
            }
        }
    }
}