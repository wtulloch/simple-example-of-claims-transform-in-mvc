﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Demo.SecurityCore
{
    public class ClaimsTransformer : ClaimsAuthenticationManager
    {
        public override ClaimsPrincipal Authenticate(string resourceName, ClaimsPrincipal incomingPrincipal)
        {
            if (!incomingPrincipal.Identity.IsAuthenticated)
            {
                return base.Authenticate(resourceName, incomingPrincipal);
            }

            var transformedPrincipal = CreateNewPrincipal(incomingPrincipal);

            return transformedPrincipal;
        }

        private ClaimsPrincipal CreateNewPrincipal(ClaimsPrincipal incomingPrincipal)
        {
            if (incomingPrincipal == null) throw new ArgumentNullException("incomingPrincipal");

            var claims = new List<Claim>();
            claims.AddRange(incomingPrincipal.Claims);

            SetAdditionalClaims(incomingPrincipal, claims);

            var identity = new ClaimsIdentity(claims, "Local Authority");

            return new ClaimsPrincipal(identity);
        }

        private void SetAdditionalClaims(ClaimsPrincipal incomingPrincipal, List<Claim> claims)
        {
            var roles = incomingPrincipal.FindAll(c => c.Type == ClaimTypes.Role)
                                         .Select(c => c.Value);

            foreach (var role in roles)
            {
                AddClaimsByRole(role, claims);
            }
            

        }

        private void AddClaimsByRole(string role, List<Claim> claims)
        {
            switch (role)
            {
                case "User":
                    claims.Add(new Claim(DemoClaimTypes.SampleApp,"View"));
                    claims.Add(new Claim(DemoClaimTypes.SampleApp, "Add"));
                    break;
            }
        }
    }
}
